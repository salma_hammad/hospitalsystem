﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace filesProject
{
    public partial class Search : Form
    {
        //Constructor:
        public Search()
        {
            InitializeComponent();
        }

        //Back button:
        private void button3_Click(object sender, EventArgs e)
        {
            //return to the 'choose' page
            Choose f2 = new Choose();
            f2.Show();
            this.Hide();
        }
        
        //'Left' Search button:
        private void button1_Click(object sender, EventArgs e)
        {
            //First thing when i click button 'Search' is that i will clear any data shown in this listview before
            listView1.Items.Clear();

            //Then take doctor name from the textbox and put it in a variable to be able to use it 
            String DoctorName = textBox1.Text;

            //Then take an object from class 'ReadSearch' to be able to call the functions written in it
            ReadSearch obj = new ReadSearch();

            //First , read data from Doctor File so , We will call Function 'Read_from_DR_file' and save 'The returned List' in 'a List of Doctors'
            List<Doctors> List = obj.Read_from_DR_file();

            //Then we will send 'The Doctor name' entered by the user , and also send the previous ' List of Doctors' which i read from Doctor file , 
            //We will send them all to Function 'Search_in_DR' which take 'The Doctor name' and 'The DR List' and return 'ID of this DR'
            int DR_ID = obj.Search_in_DR(DoctorName, List);

            //Second , read data from Patient File so , We will call Function 'Read_from_Patient_File' and save 'The returned List' in 'a List of Patients'
            List<Patient> L = obj.Read_from_Patient_File();

            //Then we will send 'The Doctor ID' , and also send the previous 'List of Patients' which i read from Patient file ,
            //We will send them all to Function 'Search_in_Patient' which takes 'The Doctor ID' and 'The Patient List' and return 'Filtered patient List'
            //'Filtered patient List' --> List contains the patients whose IDs are the same as the ID of the Entered 'Doctor Name'
            List<Patient> Filtered_Patient_List = obj.Search_in_Patient(DR_ID , L);

            //Then call function 'Display' and send to it 'The Filtered Patient List' and 'The Listview'
            Display(Filtered_Patient_List,listView1);
        }

        //'Right' Search button:
        private void button2_Click(object sender, EventArgs e)
        {
            //First thing when i click button 'Search' is that i will clear any data shown in this listview before
            listView2.Items.Clear();

            //Then take disease name from the textbox and put it in a variable to be able to use it 
            String DiseaseName = textBox2.Text;

            //Then take an object from class 'ReadSearch' to be able to call the functions written in it
            ReadSearch obj = new ReadSearch();

            //First , read data from Patient File so , We will call Function 'Read_from_Patient_File' and save 'The returned List' in 'a List of Patients'
            List<Patient> List1 = obj.Read_from_Patient_File();

            //Then we will send 'The Disease Name' , and also send the previous 'List of Patients' which i read from Patient file ,
            //So , We will send them all to Function 'Search_in_Patient' which takes 'The Disease Name' and 'List1' and returns 'List2'
            //'List2' --> List contains the patients who have the same disease as the 'entered disease name'
            List<Patient> List2 = obj.Search_in_Patient(DiseaseName, List1);

            //Then call function 'Display' and send to it 'List2' and 'The Listview2'
            Display(List2, listView2);
        }
        
        //Function 'Display' takes 2 parameters : 
        // 1) 'List of patients' which the function will display it on the Listview 
        // 2) The 'Listview' which will display on , Since i have 2 Listviews  
        private void Display (List<Patient> Filtered_Patient_List,ListView Listview)
        {
           //We will make For Loop on the 'List' which is sent to the function as a parameter 
           for(int i=0 ; i<Filtered_Patient_List.Count ; i++)
           {
               //We will call 'the getters functions' of 'the filtered patient list' , and save each of them in a variable
               string Name = Filtered_Patient_List[i].get_name();
               string ID =   Filtered_Patient_List[i].get_ID().ToString();
               string Disease = Filtered_Patient_List[i].get_disease();
               string DR_ID = Filtered_Patient_List[i].get_Dr_ID().ToString();
               string Appointment = Filtered_Patient_List[i].get_appointment();

               //i will declare that each row in the Listview will contain : Name , ID , Disease , DR ID , Appointment 
               //Then save them in 'array of strings'
               string[] row = { Name, ID, Disease, DR_ID, Appointment };

               //Then take object from class 'ListViewItem' and send the 'row' to the Listview's constructor 
               ListViewItem item = new ListViewItem(row);

               //Then Add this 'object' to ListView
               Listview.Items.Add(item);
           }
        }
    }
}
