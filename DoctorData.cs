﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace filesProject
{
    public partial class DoctorData : Form
    {
        //Constructor:
        public DoctorData()
        {
            InitializeComponent();
        }

        //Back button:
        private void button2_Click(object sender, EventArgs e)
        {
            //return to the 'choose' page
            Choose f2 = new Choose();
            f2.Show();
            this.Hide();
        }

        //Done Button:
        private void button1_Click(object sender, EventArgs e)
        { 
            //When we click 'Done' button , we will take each input entered by the user in variables to be able to use them
            string txtName = txt_name.Text;
            int txtID = int.Parse(txt_id.Text);
            string specialty = txt_sp.Text;

            //Then take object from class Doctor
            Doctors Dr = new Doctors();

            //Then send 'The Entered Data by the user'-->(name,ID,Specialty) to Function 'ReadData_FromUser' in Class 'Doctors'
            Dr.ReadData_FromUser(txtName, txtID, specialty);

            //Then write these data in the 'DoctorData' File
            Dr.Write_Dr();

            //Then take an object from 'Class Choose' to return to 'Choose' page after i press 'Done' button
            Choose obj = new Choose();
            obj.Show();
            this.Hide();
        }

        private void DoctorData_Load(object sender, EventArgs e)
        {

        }
    }
}
