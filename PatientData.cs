﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace filesProject
{
    public partial class Data : Form
    {
        //Constructor:
        public Data()
        {
            InitializeComponent();
        }

        //Back button:
        private void button2_Click(object sender, EventArgs e)
        {
            //return to the 'choose' page
            Choose C = new Choose();
            C.Show();
            this.Hide();
        }

        //Done Button:
        private void button1_Click(object sender, EventArgs e)
        {
            //When we click 'Done' button , we will take each input entered by the user in variables to be able to use them
            string Pn = P_Name.Text;
            int Pid = int.Parse(P_ID.Text);
            string Des = P_Dis.Text;
            int DR_id = int.Parse(P_Dr_ID.Text);
            string appoint = dateTimePicker1.Text;

            //Then take object from class Patient
            Patient patient = new Patient();

            //Then send 'The Entered Data by the user'-->(name,ID,Disease,DR ID,Appointment) to Function 'ReadData_FromUser' in Class 'Doctors'
            patient.GetData_FromUser(Pn , Pid , Des , DR_id , appoint);

            //Then write these data in the 'PatientData' File
            patient.Write_Patient();

            //Then take an object from 'Class Choose' to return to 'Choose' page after i press 'Done' button
            Choose obj = new Choose();
            obj.Show();
            this.Hide();
        }   
    }
}
